%% Copyright 2012 Renater
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained’.
%
% The Current Maintainer of this work is Matthieu Herrb
%
% This work consists of the files:
% jres.cls
% article.tex
%----------------------------------------------------------------------
%
% Instructions pour utiliser la classe jres :
%
% Suivre cet exemple au maximum. Règles générales :
%
% - suivre les règles de saise de french/babel concernant les
%   ponctuations doubles
% - le style de bibliographie est imposé, pas besoin de le re-définir.
%
%----------------------------------------------------------------------
% Utilisation de la classe de documents jres.
% Les parametres city et year permettent de situer le lieu et l’année
% (optionnels si la version à jour de la classe est utilisée)
\documentclass[city=Nantes,year=2017]{jres}

    % si pdflatex est utilisé, spécifier le codage du fichier source
    \usepackage[utf8x]{inputenc}

    \title{Quelle infrastructure pour dégoogliser Internet ?}

    % Utiliser la commande \auteur pour definir la liste des auteurs.
    % Le 3e élément (adresse) peut être vide

    \auteur{Luc \textsc{Didry}}
    {Association Framasoft}{%
        }

    \auteur{Pierre-Yves \textsc{Gosset}}
	{Association Framasoft}{%
	    c/o Locaux Motiv\\
	    10 bis, rue Jangot
        69\,007 \textsc{Lyon}}

    % Pas besoin de \date{}.
	\lstset{literate=%
		{à}{{\`a}}1
		{é}{{\'e}}1
	}
\begin{document}

% produit le titre selon les infos ci-dessus
\maketitle

% Résumé
\begin{abstract}
En 2014, l’association \href{https://framasoft.org}{Framasoft} s’est lancée dans une campagne intitulée « \href{https://degooglisons-internet.org/}{Dégooglisons Internet} ».
Cette campagne se caractérise notamment par la mise à disposition de services en ligne reposant uniquement sur des logiciels libres, en alternative aux services de Google, Apple, Facebook, Amazon et Microsoft (GAFAM).

Les différents sites de l’association accueillent environ deux millions de visites mensuelles (stats Piwik), soit environ 300 000 visiteurs par mois.

Entre 2014 et 2017, le nombre de services proposés au public est passé de 5 à 31.
D’une dizaine de serveurs surdimensionnés (fonctionnant sans aucune supervision), l’association est passée à 24 serveurs physiques hébergeant une cinquantaine de machines virtuelles, aujourd’hui gérés à plein temps par un administrateur système.
L’association héberge près d’une centaine de services différents (Drupal, Wordpress, Mediawiki, Odoo, Discourse, Jitsi Meet, Mastodon, diaspora*, Etherpad…)
Les technologies se sont diversifiées : à Apache, PHP, NodeJS, Java, Mysql et Redis se sont ajoutés Nginx, Varnish, Perl, Ruby, Python, Lisp, PostgreSQL, RethinkDB, Docker… pour les seuls services publiquement accessibles.
À cela s’ajoutent les problématiques de supervision, sauvegarde, automatisation, croissance des services…

L’infrastructure a dû être profondément remaniée pour absorber cette croissance, non seulement en termes de nombre de services, mais aussi de fréquentation.
\end{abstract}

\motscles{Infrastructure, Framasoft, Dégooglisons, Virtualisation, Hébergement, AdminSys}

\section{Introduction}


Framasoft est un réseau d’éducation populaire créé en novembre 2001 et épaulé depuis 2004 par une association éponyme.
Ce réseau s’est d’abord constitué autour d’un annuaire de logiciels libres et gratuits sous Windows puis uniquement libres à partir de 2004 \cite{wikipedia-frama}.

Au cours du temps, des services se sont ajoutés à cet annuaire : un \href{https://framagora.org}{forum} qui fut particulièrement utilisé lors des débats sur la loi \href{https://fr.wikipedia.org/wiki/Loi_relative_au_droit_d%27auteur_et_aux_droits_voisins_dans_la_soci%C3%A9t%C3%A9_de_l%27information}{DAVDSI}, des \href{https://framakey.org}{clés USB contenant une distribution GNU/Linux et des logiciels portables}, une maison d’édition d’ouvrages libres…

En 2011, deux services en ligne sont mis à disposition de tout un chacun : un \href{https://framapad.org}{éditeur de texte collaboratif en temps réel} basé sur \href{http://etherpad.org/}{Etherpad}, un \href{https://framacalc.org}{tableur collaboratif} basé sur \href{https://ethercalc.org/}{Ethercalc} et un \href{https://framadate.org}{service de sondage} spécialisé dans la planification de rendez-vous, basé sur un \emph{fork} du logiciel \href{https://sourcesup.cru.fr/projects/studs/}{Studs}.

Constatant que les usages ont changé, déplaçant le souci du logiciel libre de l’ordinateur aux services en ligne où les GAFAM se taillent la part du lion, l’association a lancé en 2014 la campagne « Dégooglisons Internet ».
Cette campagne vise à proposer des alternatives uniquement basée sur des logiciels libres aux services en ligne propriétaires et centralisateurs de données.

Cette initiative avait été précédée en 2013 par la campagne « \href{https://framablog.org/2013/11/21/framasoft-campagne-soutien-2013/}{Moins de Google et plus de Libre} » visant à dégoogliser… Framasoft.
En effet, l’association était grande consommatrice des services Google : mails, listes de discussions, statistiques des sites, google docs…
Cette opération de \emph{dégooglisation} interne fut l’occasion d’une restructuration complète de l’infrastructure de Framasoft permise notamment par l’arrivée d’un administrateur système bénévole quelques mois auparavant.

Cette restructuration fut pensée pour répondre à un certain nombre de problématiques :
\begin{itemize}
    \item rationalisation des ressources ;
    \item flexibilité ;
    \item disponibilité ;
    \item automatisation ;
    \item simplicité d’administration (il n’y a que très peu de personnes possédant des compétences en administration système au sein de Framasoft) ;
\end{itemize}

Nous présenterons les solutions techniques utilisées pour mettre en place et gérer notre infrastructure, celles qui ont été écartées ainsi que les problèmes rencontrés au cours de cette aventure et nous montrerons qu’il est possible, avec des moyens limités, de déployer dans l’ESR des outils performants évitant une "fuite" des données vers les services \emph{cloud} des principaux acteurs d’Internet.

NB : Les distributions GNU/Linux utilisées sont toutes des Debian, de Wheezy à Stretch, plus deux Ubuntu (pour les besoins spécifiques des logiciels installés).

\section{Automatisation}


Si monter un nouveau serveur est simple et rapide, la configuration de celui-ci nécessite toujours du temps.

En effet, créer les comptes des administrateurs, installer nos logiciels habituels, éventuellement les configurer, ajouter un agent de supervision, mettre à jour le serveur de supervision… tout cela est éminemment chronophage.

Certes, un ou plusieurs scripts peuvent résoudre ce problème, mais \emph{quid} de l’évolution des configurations ? De la maintenabilité d’un tel script ?

Nous nous sommes alors tournés vers les logiciels de gestion de configuration.

\subsection{Logiciels candidats}

L’offre de tels logiciels dans le monde du libre est \href{https://en.wikipedia.org/wiki/Comparison_of_open-source_configuration_management_software}{pléthorique}.

Si l’administrateur système connaissait bien \href{https://puppet.com/}{Puppet}, sa courbe d’apprentissage n’aurait pas permis aux autres membres de l’association impliqués dans la technique d’être rapidement à même d’écrire des recettes.
\href{http://www.ansible.com/}{Ansible}, logiciel alors encore jeune, n’ayant pas fonctionné correctement lors des tests, fut lui aussi écarté.

Ce fut \href{https://saltstack.com/}{Salt} qui fut finalement choisi.
En effet, la déclaration des recettes est faite en \href{https://fr.wikipedia.org/wiki/YAML}{YAML}, un langage de représentation de données fort simple. De plus, sa documentation est très fournie. Ces deux facteurs ont permis à l’équipe de se l’approprier rapidement.

\subsection{Fonctionnement}
À l’instar de Puppet, Salt nécessite l’installation d’un agent (appelé \emph{minion}) sur les serveurs.

Mais contrairement à Puppet où le maître va se connecter aux agents pour lui transmettre des commandes, ce sont les \emph{minions} qui vont se connecter au maître, connexion qui restera active (et relancée en cas de coupure).
Cela permet une grande réactivité des minions, avantage non négligeable au fur et à mesure de l’augmentation de leur nombre.

Les communications entre le maître et les minions sont bien évidemment chiffrées.

Les recettes, écrites en YAML, sont interprétées comme des modèles \href{http://jinja.pocoo.org/}{Jinja}, permettant ainsi de créer des boucles, des conditionnelles, etc.

De plus, Salt intègre nativement la gestion de données sous forme clé/valeur (dans un composant appelé \emph{pillar}), là où Puppet nécessitait l’emploi d’un composant externe, \emph{Hiera}.

Les recettes Salt permettent ainsi d’utiliser des données définies par l’administrateur dans les pillars.
Rien de plus simple, dès lors, de créer à la volée tous les comptes nécessaires sur une machine avec une boucle dans la recette et la description des comptes dans les pillars.

Dans les pillars :
\begin{lstlisting}
admins:
  foo:
    fullname: Foo Bar
    shell: /bin/zsh
    key: XXXX
    password: XXXX
\end{lstlisting}

Dans la recette :
\begin{lstlisting}
{% for login, user in pillar.get('admins', {}).items() %}
{{login}}:
  user.present:
    - home: /home/{{login}}
    - createHome: True
    - fullname: {{user.fullname}}
{% if user.shell is defined %}
    - shell: {{user.shell}}
{% endif %}
    - password: {{user.password}}
{{login}}_ssh:
  ssh_auth:
    - present
    - user: {{login}}
    - name: {{user.key}}
{% endfor %}
\end{lstlisting}

De même, les fichiers (comme des scripts maison) que l’on voudrait envoyer sur les serveurs peuvent être interprétés comme des modèles Jinja et peuvent accéder aux données des pillars.

Mais les pillars ne sont pas les seules sources de données disponibles !
Les minions peuvent renvoyer des informations relatives à la machine, appelées \emph{grains}, comme le type et la version du système d’exploitation, la version du noyau, s’il s’agit d’une machine virtuelle ou physique…

Exemple de grains :

\begin{lstlisting}
    kernel:
        Linux
    kernelrelease:
        4.9.0-0.bpo.2-amd64
    locale_info:
        ----------
        defaultencoding:
            UTF-8
        defaultlanguage:
            en_US
        detectedencoding:
            UTF-8
    lsb_distrib_codename:
        jessie
    lsb_distrib_description:
        Debian GNU/Linux 8.9 (jessie)
    lsb_distrib_id:
        Debian
    lsb_distrib_os:
        GNU/Linux
    lsb_distrib_release:
        8.9
\end{lstlisting}

Ces grains pourront être utilisés de la même façon que les pillars dans les recettes :

\begin{lstlisting}
{% if grains["os"] == 'RedHat' %}
httpd:
  pkg:
    - installed
{% elif grains["os"] == 'Debian' %}
apache2:
  pkg:
    - installed
{% endif %}
\end{lstlisting}

Les instructions sont poussées par le maître aux minions, ce ne sont pas les minions qui vont interroger le maître.

Il est possible d’appliquer à une machine l’ensemble des recettes avec une simple commande, mais il est aussi possible de n’en appliquer qu’un sous-ensemble (très pratique lors d’une simple mise à jour d’un service) :

\begin{lstlisting}
# On utilise l'ensemble des recettes
salt server state.highstate
# On applique qu'une seule recette
# par exemple pour mettre à jour la configuration de Shinken
salt server state.sls monitoring
\end{lstlisting}

Il est aussi possible d’exécuter des fonctions de Salt sans pour autant écrire une recette :

\begin{lstlisting}
salt server user.add name <uid> <gid> <groups> <home> <shell>
\end{lstlisting}

L’utilisation de Salt au sein de Framasoft a donné lieu à la création d’une \href{https://slides.fiat-tux.fr/slides_salt/index.html}{présentation} utilisée pour transmettre rapidement les bases de Salt à l’équipe technique de Framasoft (ainsi qu’à l’équipe réseau de l’université de Lorraine, employeur d’alors de l’administrateur bénévole).

\section{Flexibilité}

\subsection{Virtualisation}

Les impératifs de rationalisation des ressources et de flexibilité de l’infrastructure nous ont amenés à nous pencher sur le sujet de la virtualisation.
En effet, la virtualisation permet de créer ou supprimer des machines en quelques commandes, et de revoir au besoin les ressources allouées aux machines virtuelles au fur et à mesure de l’évolution des services… ou tout simplement en cas de sur-dimensionnage à la création.

\subsubsection{Logiciels candidats}

Bien évidemment, l’utilisation de solutions propriétaires telles que VMware ou Amazon Elastic Compute Cloud étaient hors de question.
Utiliser le service de virtualisation d’un hébergeur, même utilisant une solution libre ne nous convenait pas non plus : rien ne garantit que la solution de virtualisation utilisée ne risque pas de changer un jour pour une solution propriétaire.

Il nous fallait donc opérer nous-mêmes notre propre solution de virtualisation.
Nous avons, après quelques recherches, sélectionné quatre candidats à tester :
\begin{itemize}
    \item \href{http://www.ganeti.org/}{Ganeti} ;
    \item \href{https://libvirt.org/}{Libvirtd} ;
    \item \href{https://pve.proxmox.com/wiki/Main_Page}{Proxmox}
    \item \href{https://www.openstack.org/software/}{OpenStack}.
\end{itemize}

OpenStack a vite montré son inadéquation avec notre projet de par sa complexité et ses exigences en matériel pour monter un cluster basique.

De même, Proxmox a été rapidement mis de côté : son installation a été plus que problématique et n’a pas fonctionné.
Le manque de temps pour les tests a contribué à cette mise à l’écart.
Rappelons que la refonte de l’infrastructure devait aller le plus vite possible afin de commencer la \emph{dégooglisation} de l’association rapidement : les tests ont duré approximativement deux mois avec un administrateur système bénévole (donc opérant sur son temps libre).

Ganeti l'a emporté face à Libvirtd grâce sa documentation concise mais complète, ainsi que par sa gestion aisée d’un \emph{cluster} de virtualisation.

\subsubsection{Mise en place et fonctionnement (succinct)}

La mise en place initiale s’est faite sur deux serveurs afin de redonder les disques des machines virtuelles grâce à \emph{Distributed Replicated Block Device} (DRBD).

Plutôt que de gérer l’accès aux services des machines virtuelles via un \emph{reverse proxy}, nous avons préféré louer un sous-réseau IPv4 supplémentaire afin de fournir une adresse IPv4 publique à chaque machine virtuelle.

Un cluster Ganeti comporte des nœuds (les serveurs physiques), lesquels peuvent être rassemblés en groupes, des réseaux d’adresses privées ou publiques qui seront assignées aux machines virtuelles (ces réseaux seront rattachés à des groupes de nœuds s’il y en a), et des instances (les machines virtuelles).

Un des nœuds sera le \emph{master} du cluster, élu par les autres nœuds du cluster et permettra de gérer le cluster.
On peut bien évidemment forcer le choix du \emph{master}.

Il est possible d’accéder à une console VNC pour chaque instance.
Celle-ci écoute sur l’interface locale du nœud hébergeant l’instance.

Il est à noter que les instances ne peuvent changer de nœud qu’au sein d’un même groupe.

\subsubsection{Problèmes rencontrés}

Si redonder les disques des instances sur un nœud secondaire est intéressant au cas où le nœud primaire rencontrerait une malfonction, l’impact de DRBD sur les performances est notable, même si, comme c’était notre cas, les nœuds sont situés dans le même centre de données.

Cette baisse de performances impactant significativement l’expérience utilisateur de nos usagers, nous avons décidé de nous passer de cette sécurité.
En cas de problème insoluble sur un nœud, les instances seront recrées et les services remontés à partir des sauvegardes.

Il s’agit là d’un risque mesuré : les nœuds utilisent tous des disques en RAID 1, plusieurs solutions de supervision sont en place, nous permettant généralement d’anticiper les pannes et notre hébergeur (Hetzner) est très réactif.

\subsection{Stockage flexible}

L’utilisation croissante de certains services fort consommateurs d’espace disque nous a amené à nous pencher sur la problématique du stockage distribué.
En effet, si la charge du serveur restait la même, le disque, lui, se remplissait inlassablement.
Or, s’il est possible d’ajouter de l’espace disque à une machine virtuelle, on reste néanmoins tributaire de l’espace disponible sur le serveur hôte… et les autres machines virtuelles ont elles aussi besoin d’utiliser des disques, réduisant forcément d’autant l’espace disponible.

La migration d’un service gourmand en espace disque est possible, mais outre le fait de se retrouver avec une machine surdimensionnée en terme de processeur et de RAM, le risque est présent de se heurter de nouveau à la limite de l’espace disque disponible sur le serveur.

Il a donc été décidé de s’orienter vers des systèmes de fichiers distribués.

\subsubsection{Choix du logiciel}

L’offre en logiciels libres proposant cette fonctionnalité est bien fournie, entre \href{https://ceph.com}{Ceph}, \href{http://www.gluster.org/}{GlusterFS}, \href{http://lustre.org/}{Lustre} et consorts.
Notre attention a très vite été attirée par Ceph dont une des particularités est de ne pas présenter de point unique de défaillance (\emph{single point of failure}).

Le test de Ceph fut concluant, que ce soit en terme de simplicité de mise en place avec \texttt{ceph-deploy} ou en terme de performances.

Les autres solutions n’ont du coup pas fait l’objet de tests.

\subsubsection{Infrastructure}
Pour des raisons de coût, le test a été effectué sur une unique machine, possédant deux disques SSD en RAID1 et deux disques mécaniques de 3To.

Notre cluster de test était composé d’un dæmon \emph{monitor} (responsable — entre autre — de l’orchestration du cluster) et de deux dæmons \emph{osd} (servant au stockage).
Les deux disques mécaniques étaient dédiés au stockage (utilisés par un dæemon \emph{osd} chacun) tandis que les disques SSD hébergeaient le système d’exploitation ainsi que les « journaux » des \emph{osd} (un buffer stockant temporairement les données avant qu’elles ne soient envoyées sur les disques mécaniques).

Il a été fort simple de forcer Ceph à fonctionner correctement sur deux disques mécaniques d’un même serveur en modifiant la configuration de la \emph{crushmap} du cluster (responsable de la répartition des données).

Après les tests, nous avons intégré un autre serveur équivalent dans le cluster, re-modifié les règles de répartition de façon à ce que les données soient redondées entre deux serveurs et non entre deux disques du même serveur.
Le nouveau serveur comporte donc lui aussi deux dæmons \emph{osd} et un dæmon \emph{monitor}.

Afin d’avoir trois dæmons \emph{monitor} dans notre cluster, nous avons déployé un \emph{monitor} sur une machine virtuelle de notre infrastructure.
Trois \emph{monitor} est le nombre minimal requis par Ceph pour garantir la redondance et la haute disponibilité du cluster\cite{ceph-mon}.

\subsubsection{Problèmes rencontrés}

Nous avions tout d’abord utilisé le cluster pour créer des partitions formatées en ext4 mais nous avons rencontré de graves problèmes de stabilité.
L’utilisation du système de fichiers xfs a résolu ce problème

L’utilisation du module noyau \emph{rbd} pour accéder aux volumes Ceph sur le serveur s’est révélé problématique lorsque le serveur n’utilisait pas le dernier noyau installé (cas d’une mise à jour noyau effectuée mais serveur non redémarré).
L’utilitaire \emph{rbd-nbd} nous a permit de contourner ce problème.

Les serveurs sous Stretch, de par leur noyau plus récent, ne permettent pas d’accéder aux volumes.
Le problème a été résolu en amont mais aucune mise à jour intégrant le \emph{fix} n’était publiée au moment de la rédaction de cet article.

\section{Disponibilité}

Les services de Framasoft ne répondent pas à un besoin de fourniture de services mais constituent une démonstration de faisabilité.
À ce titre, la haute disponibilité n’est pas un enjeu majeur pour l’association.
Aucune solution n’a été déployée en ce sens.

Cependant, cela ne signifie bien évidemment pas que rien n’a été préparé pour permettre une reprise d'exploitation la plus rapide possible d’un service ou d’un serveur.

\subsection{Sauvegardes}

Malheureusement trop souvent la cinquième roue du carrosse d’une infrastructure informatique, les sauvegardes constituent pourtant la pierre angulaire d’un plan de reprise d’activité.

Nous utilisons plusieurs logiciels de sauvegardes, selon les logiciels utilisés, la volumétrie ou le type des données à sauvegarder.

\subsubsection{Backuppc}

\href{http://backuppc.sourceforge.net/}{Backuppc} fut le premier logiciel de sauvegarde installé.
Bien qu’il ait un aspect vieillot, il reste puissant et flexible et fort simple à installer via les paquets Debian.
Son développement semblait plus que ralenti depuis décembre 2015 (date de sortie de la version 3.3.1, version proposée par la version stable actuelle de Debian) mais une nouvelle version majeure est sortie début 2017, ainsi qu’une version de maintenance de la version 3.3.
Gageons que Backuppc est encore là pour longtemps !

Le choix de Backuppc fut relativement rapide : en effet, son interface web le rend accessible aux moins techniques des membres de l’association, tout du moins en ce qui concerne la restauration des fichiers.
La restauration peut s’effectuer directement sur la machine sauvegardée, dans le répertoire d’origine ou non, ou sur un autre serveur auquel Backuppc peut accéder. Enfin, l’opérateur peut simplement télécharger le fichier sur son ordinateur.

Notre politique de sauvegarde via Backuppc est relativement simple : une fois par semaine, Backuppc effectue une sauvegarde complète des serveurs, et pendant le reste de la semaine, ce seront des sauvegardes incrémentielles qui seront effectuées.

L’utilisation des commandes pré et post-sauvegardes nous permettent d’effectuer un dump de nos bases de données MySQL afin de les inclure dans nos sauvegardes.

Les sauvegardes sont initiées par le serveur hébergeant Backuppc (méthode de sauvegarde dite \emph{pull}).

Un script généré par Salt permet de vérifier que tous les serveurs de l’association sont bien enregistrés dans la configuration de Backuppc.

\subsubsection{Borg}

En ce qui concerne les services à forte volumétrie (hébergement de fichiers (Framadrive, Framadrop), d’images (Framapic)…), nous avons préféré recourir à une autre solution.
En effet, ces sauvegardes représentent plusieurs téraoctets de fichiers et nécessitent énormément de temps, ce qui bloquerait les sauvegardes des autres serveurs via Backuppc (celui-ci n’effectuant qu’un petit nombre de sauvegardes en parallèle afin de ne pas surcharger le serveur de sauvegarde).

\href{https://borgbackup.readthedocs.io/}{Borg} a pour avantages d’être simple à utiliser au sein de scripts shell et de n’effectuer que des sauvegardes par déduplication (aucune sauvegarde complète n’est faite si ce n’est la première).

De même, les commandes sont simples à utiliser et très bien documentée, proposant ainsi une barrière d’entrée relativement faible pour les non-techniciens.

Si Borg utilise la méthode push pour effectuer les sauvegardes, celle-ci ne souffre pas du problème habituel de cette méthode : le risque d’effacement des anciennes sauvegardes par un attaquant s’étant rendu maître de la machine sauvegardée.
En effet, les dépôts de sauvegardes peuvent être configurés pour n’autoriser que l’ajout de sauvegardes.
Un script sur la machine hébergeant les dépôts pourra faire sauter ce verrou dans le fichier de configuration pour effectuer la suppression des sauvegardes puis remettre le verrou.

Les sauvegardes de Borg peuvent être chiffrées, ce que nous avons activé : ces sauvegardes sont stockées sur une \emph{storage box} de notre hébergeur, une offre ne proposant que de l’espace de stockage avec un rapport volumétrie/prix que nous ne pouvons atteindre en louant des serveurs.

\subsubsection{Duplicity}

\href{http://duplicity.nongnu.org}{Duplicity} était la solution de sauvegarde de forte volumétrie que nous utilisions avant Borg.
Moins stable que Borg, il nécessitait aussi un espace de cache important sur le serveur sauvegardé (le dossier de cache pesait près de 200Gio pour 2Tio de fichiers à sauvegarder, là où Borg ne consomme que 8Gio pour la même sauvegarde).

\subsubsection{Barman}

Les bases de données MySQL sont, on l’a vu, sauvegardées par Backuppc.

Quid des bases de données PostgreSQL ?
Nous aurions pu utiliser la même statégie que pour MySQL, mais la volumétrie de nos bases de données PostgreSQL (certains de nos clusters PostgreSQL utilisent près de 90Gio d’espace disque) auraient fortement allongé le temps de sauvegarde, sans compter que certaines de ces bases sont massivement sollicitées (comme celles de nos instances Framapad par exemple) et un blocage des bases le temps du dump n’aurait pas été sans impact pour nos utilisateurs.

\href{http://www.pgbarman.org/}{Barman} est un outil de sauvegarde à chaud de cluster PostgreSQL (et non pas de bases de données).
Le cluster enverra au fur et à mesure ses \href{https://en.wikipedia.org/wiki/Write-ahead_logging}{\emph{Write Ahead Log}} (WAL) au serveur sur lequel tourne Barman, et celui-ci effectuera une fois par jour des sauvegardes via la commande \texttt{pg\_basebackup} fournie par PostgreSQL (celle-ci n’affecte pas les clients) qui serviront de base sur laquelle rejouer les WAL.

Grâce aux WAL, il est possible de remonter un cluster PostgreSQL dans l’état dans lequel il était au moment choisi (contrairement aux dumps qui ne permettent de remonter une base de données qu’en l’état de l’heure du dump).

Robuste et fiable, Barman permet aussi de restaurer \emph{localement} un cluster, ce qui permet de le lancer sur le serveur de sauvegarde et donc d’effectuer un dump d’une unique base de données, de récupérer un unique enregistrement, etc.

\subsection{Monitoring et métrologie}

Si les sauvegardes sont importantes, la surveillance de l’infrastructure l’est tout autant.

Le monitoring permet de détecter rapidement les pannes ou les comportements anormaux tandis que la métrologie permet un suivi dans le temps de l’infrastructure, ce qui simplifie la détection de futurs problèmes (occupation croissante de l’espace disque par exemple) ou la vérification de l’impact de certaines actions (sur la charge serveur ou la consommation de mémoire vive par exemple).

\subsubsection{Monitoring}

Le monitoring est effectué par le logiciel Shinken et utilise des agents NRPE installés sur chacun des serveurs pour les sondes nécessitant une exécution locale.

Shinken a l’avantage d’utiliser les mêmes sondes que Nagios, sondes nombreuses et fournies par la grande majorité des distributions GNU/Linux, ainsi que — et c’est là une des plus-values de Shinken sur Nagios — la possibilité native de mettre en place une architecture distribuée pour le monitoring.

La configuration des sondes (hôtes, sites web, etc) est gérée par Salt grâce aux pillars : il suffit d’ajouter les informations idoines et de pousser la configuration sur le serveur Shinken :

\begin{lstlisting}
# entrée du serveur
foo:
  # déclaration des sites web
  http:
    foo.framasoft.org:
      # celui-ci est en HTTPS
      - https: True
  # le serveur contient un serveur PostgreSQL
  postgresql: True
  # surcharge des seuils d'alarme de la sonde check_disk
  shinken:
    check_disk:
      - w: 4
      - c: 2
\end{lstlisting}

\newpage
\begin{lstlisting}
# On pousse la configuration sur le serveur de monitoring
salt server_monitoring state.sls monitoring
# On pousse la configuration des sondes locales
# (exécutées par NRPE) sur le serveur foo
salt foo state.sls common.monitoring
\end{lstlisting}

L’utilisation de Salt apporte non seulement une syntaxe simplifiée mais également de centraliser dans un seul fichier les configurations de Shinken et des sondes locales.

\subsubsection{Métrologie}

Pour la métrologie, c’est le vénérable Munin qui a été choisi pour sa simplicité et son extensibilité. Rien de plus simple en effet que de développer une nouvelle sonde Munin !

Nous avons aussi mis en place Netdata mais sans rétention des données : cet outil servira plutôt à une analyse en temps réel d’un serveur qu’à une surveillance des évolutions ou qu’aux analyses post-mortem des problèmes.

\section{Conclusion}

Il est bien possible, avec du temps et de la volonté, de mettre en place une infrastructure solide permettant de fournir un grand nombre de services différents à un grand nombre de personnes, et ce, uniquement avec des logiciels libres (si l’on oublie quelques pilotes matériéls propriétaires parfois nécessaires).

Un accent a été mis sur l’accessibilité de la plupart des solutions aux personnes ne disposant pas d’expérience approfondie de l’administration système, ce qui permet une autonomisation des intervenants et la baisse de la pression sur l’administrateur.

NB : Il est d’autres aspects qui auraient pu être traités ici, comme la sécurité, la nécéssité d’une documentation constamment mise à jour, etc., mais qui auraient débordés de l’espace autorisé pour cet article.


% Bibliographie
\nocite{*}
\bibliography{article}

\end{document}
